#!/bin/bash

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}

run 'xcape -e Control_L=Super_L|F12'
run 'xcape -e Control_R=Super_L|F12'
run 'xcape -e Super_L=Super_L|F12'
