# Plugin
# ──────
plug "alexherbo2/explore.kak"
plug "alexherbo2/prelude.kak"
plug "alexherbo2/auto-pairs.kak"
plug "danr/kakoune-easymotion"
plug "delapouite/kakoune-palette"
plug "alexherbo2/pager.kak"
plug "occivink/kakoune-expand" %{
    declare-user-mode expand
    map global expand <tab> ': expand<ret>' -docstring "expand"
    map global user <tab> ': expand; enter-user-mode -lock expand<ret>' -docstring "expand ↻"
}
plug "Delapouite/kakoune-expand-region" %{
map global normal <c-e> :expand<ret>
}
plug "alexherbo2/connect.kak" %{
    # require-module connect-fzf
    # require-module connect-dmenu
    # require-module connect-rofi
    # require-module connect-wofi
    # require-module connect-lf
    # require-module connect-dolphin
}
plug "andreyorst/fzf.kak" %{
    set-option global fzf_file_command 'rg'
    set-option global fzf_file_command "find . \( -path '*/.svn*'
    	-o -path '*/.git*' \) -prune -o -type f -print"
    set-option global fzf_highlight_command 'bat'
}
plug 'delapouite/kakoune-select-view' %{
    map global normal <a-%> ': select-view<ret>' -docstring 'select view'
    map global view s '<esc>: select-view<ret>' -docstring 'select view'
}
plug "alexherbo2/move-line.kak" %{
    map global normal "<a-down>" ': move-line-below<ret>'
    map global normal "<a-up>" ': move-line-above<ret>'
}
plug 'delapouite/kakoune-text-objects' %{
  text-object-map
}
plug 'delapouite/kakoune-mirror' %{
  # Suggested mapping
  map global normal "'" ': enter-user-mode -lock mirror<ret>'
}
plug "eraserhd/parinfer-rust" do %{
    cargo install --force --path .
} config %{
    hook global WinSetOption filetype=(clojure|lisp|scheme|racket) %{
        parinfer-enable-window -smart
    }
}
