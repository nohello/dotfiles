# Load local Kakoune config file if it exists
# ───────────────────────────────────────────

evaluate-commands %sh{ [ -f $kak_config/local.kak ] && echo "source $kak_config/local.kak" }

# Source
# ──────
source "%val{config}/plugins/plug.kak/rc/plug.kak"
source "%val{config}/config.kak"
source "%val{config}/packages.kak"

# Key mappings
# ────────────

# Clipboard map
# ─────────────

evaluate-commands %sh{
    case $(uname) in
        Linux) copy="xclip -i"; paste="xclip -o" ;;
        Darwin)  copy="pbcopy"; paste="pbpaste" ;;
    esac

    printf "map global user -docstring 'paste (after) from clipboard' <c-v> '!%s<ret>'\n" "$paste"
    printf "map global user -docstring 'paste (after) from clipboard' p '!%s<ret>'\n" "$paste"
    printf "map global user -docstring 'paste (before) from clipboard' P '<a-!>%s<ret>'\n" "$paste"
    printf "map global user -docstring 'yank to primary' y '<a-|>%s<ret>:echo -markup %%{{Information}copied selection to X11 primary}<ret>'\n" "$copy"
    printf "map global user -docstring 'yank to clipboard' Y '<a-|>%s<ret>:echo -markup %%{{Information}copied selection to X11 clipboard}<ret>'\n" "$copy -selection clipboard"
    printf "map global user -docstring 'replace from clipboard' R '|%s<ret>'\n" "$paste"
}

# Random keymaps
# ──────────────

map global user -docstring 'next lint error' n ':lint-next-error<ret>'
map global normal <c-p> :lint<ret>

hook global -always BufOpenFifo '\*grep\*' %{ map -- global normal - ': grep-next-match<ret>' }
hook global -always BufOpenFifo '\*make\*' %{ map -- global normal - ': make-next-error<ret>' }

# Enable <tab>/<s-tab> for insert completion selection
# ────────────────────────────────────────────────────

hook global InsertCompletionShow .* %{ map window insert <tab> <c-n>; map window insert <s-tab> <c-p> }
hook global InsertCompletionHide .* %{ unmap window insert <tab> <c-n>; unmap window insert <s-tab> <c-p> }

# Add bc integration
# ──────────────────
define-command -hidden -params 2 inc %{
  evaluate-commands %sh{
    if [ "$1" = 0 ]
    then
	count=1
    else
	count="$1"
    fi
    printf '%s%s\n' 'exec h"_/\d<ret><a-i>na' "$2($count)<esc>|bc<ret>h"
  }
}

# Tags
# ────
hook global KakBegin .* %{
    evaluate-commands %sh{
        path="$PWD"
        while [ "$path" != "$HOME" ] && [ "$path" != "/" ]; do
            if [ -e "./tags" ]; then
                printf "%s\n" "set-option -add current ctagsfiles %{$path/tags}"
                break
            else
                cd ..
                path="$PWD"
            fi
        done
    }
}

# LSP
# ───
eval %sh{kak-lsp --kakoune -s $kak_session}
hook global WinSetOption filetype=(rust|python|go|c|cpp) %{
    lsp-enable-window
}

# Normal map
# ──────────
map global normal <space> , -docstring 'leader'
map global normal <backspace> <space> -docstring 'remove all sels except main'
map global normal <a-backspace> <a-space> -docstring 'remove main sel'
map global normal D '<a-l>d' -docstring 'delete to end of line'
map global normal Y '<a-l>y' -docstring 'yank to end of line'
map global normal '#' :comment-line<ret> -docstring 'comment line'
map global normal <c-a> ':inc %val{count} +<ret>'
map global normal <c-x> ':inc %val{count} -<ret>'
map global normal = :format<ret> -docstring 'format buffer'
map global normal + ':prompt math: %{exec "a%val{text}<lt>esc>|bc<lt>ret>"}<ret>'
map global normal , <a-i> -docstring 'select inner object'
map global normal <a-,> <a-a> -docstring 'select surrounding object'

# Prefix
# ──────
map global user f ': fzf-mode<ret>' -docstring 'FZF'
map global user l ': enter-user-mode lsp<ret>' -docstring 'LSP'
map global user z ': enter-user-mode selectors<ret>' -docstring 'SELECTOR'

# User map
# ────────
map global user <backspace> 'ga' -docstring 'previous buffer'
map global user w ': w<ret>' -docstring 'write file'
map global user q ': q<ret>' -docstring 'quit file'
map global user Q ': q!<ret>' -docstring 'quit! file'
map global user x ': wq<ret>' -docstring 'write file and quit'
map global user i ': ide<ret>' -docstring 'ide'
map global user n ': new<ret>' -docstring 'new window'
map global user e ': connect-terminal<ret>' -docstring 'launch terminal'
map global user d ': connect-shell dolphin<ret>' -docstring 'launch dolphin'
map global user D ': db<ret>' -docstring 'delete current buffer'

# fzf map
# ───────
map global user <space> ': fzf-mode<ret>v' -docstring 'find file in project'
map global user b ': fzf-mode<ret>b' -docstring 'switch buffer'

# easy easymotion
# ───────────────
map global normal <c-down> ': easy-motion-alt-f<ret>'
map global normal <c-up> ': easy-motion-f<ret>'
map global normal <ret> ': easy-motion-j<ret>'
map global normal <c-k> ': easy-motion-k<ret>'
map global normal <c-b> ': easy-motion-b<ret>'
map global normal <c-B> ': easy-motion-B<ret>'
map global normal <c-w> ': easy-motion-w<ret>'
map global normal <c-W> ': easy-motion-W<ret>'

# Goto map
# ────────
map global goto m '<esc>m;' -docstring 'matching char'

# Control map
# ───────────

# Function keys map
declare-user-mode toggle
map global user t ': enter-user-mode toggle<ret>' -docstring 'toggle'
declare-user-mode toggle-color
map global toggle t ': enter-user-mode toggle-color<ret>' -docstring 'theme/colorscheme'
map global toggle-color t  ': colorscheme ' -docstring 'switch theme'
map global toggle-color 1  ': colorscheme solarized-light<ret>' -docstring 'solarized-light'
map global toggle-color 2  ': colorscheme solarized-dark<ret>' -docstring 'solarized-dark'
map global toggle-color 0  ': colorscheme default<ret>' -docstring 'default terminal color'
