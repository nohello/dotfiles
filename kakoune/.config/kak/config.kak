# User preference
# ───────────────

set-option global makecmd 'make -j9'
set-option global grepcmd 'rg --column'
set-option global ui_options ncurses_assistant=cat

# colorscheme solarized-light-termcolors
# colorscheme solarized-dark
# colorscheme default
colorscheme dracula

add-highlighter global/ show-matching
# add-highlighter global/ dynregex '%reg{/}' 0:+u
add-highlighter global/ wrap

hook global WinCreate ^[^*]+$ %{ add-highlighter window/ number-lines -hlcursor -relative -separator ' '}

# Enable editor config
# ────────────────────

hook global BufOpenFile .* %{ editorconfig-load }
hook global BufNewFile .* %{ editorconfig-load }

# Filetype specific hooks
# ───────────────────────

hook global WinSetOption filetype=(c|cpp) %{
    clang-enable-autocomplete
    clang-enable-diagnostics
    alias window lint clang-parse
    alias window lint-next-error clang-diagnostics-next
}

hook global WinSetOption filetype=(javascript|typescript) %{
    set-option buffer indentwidth 2
    set-option buffer tabstop 2
    set-option global lintcmd 'eslint'
}

hook global WinSetOption filetype=python %{
    jedi-enable-autocomplete
    lint-enable
    set-option global lintcmd 'flake8'
}

map -docstring "xml tag objet" global object t %{c<lt>([\w.]+)\b[^>]*?(?<lt>!/)>,<lt>/([\w.]+)\b[^>]*?(?<lt>!/)><ret>}

# Highlight the word under the cursor
# ───────────────────────────────────

# declare-option -hidden regex curword
# # Change rgb if color scheme is changed
# # set-face global CurWord default,rgb:eee8d5
# set-face global CurWord default,rgb:083496

# hook global NormalIdle .* %{
#     eval -draft %{ try %{
#         exec <space><a-i>w <a-k>\A\w+\z<ret>
#         set-option buffer curword "\b\Q%val{selection}\E\b"
#     } catch %{
#         set-option buffer curword ''
#     } }
# }
# add-highlighter global/ dynregex '%opt{curword}' 0:CurWord

# Helper commands
# ───────────────

define-command find -menu -params 1 -shell-script-candidates %{ ag -g '' --ignore "$kak_opt_ignored_files" } %{ edit %arg{1} }

define-command mkdir %{ nop %sh{ mkdir -p $(dirname $kak_buffile) } }

define-command ide -params 0..1 %{
    try %{ rename-session %arg{1} }

    rename-client main
    set-option global jumpclient main

    # new rename-client tools
    # set-option global toolsclient tools

    # new rename-client docs
    # set-option global docsclient docs

    connect-terminal
    connect-terminal
    connect-terminal
}

define-command delete-buffers-matching -params 1 %{
    evaluate-commands -buffer * %{
        evaluate-commands %sh{ case "$kak_buffile" in $1) echo "delete-buffer" ;; esac }
    }
}

declare-option -hidden str swap_buffer_target
define-command swap-buffer-with -override -params 1 -client-completion %{
    set-option global swap_buffer_target %val{bufname}
    edit -scratch # release current window for other client
    evaluate-commands -client %arg{1} "
        set-option global swap_buffer_target %%val{bufname}
        buffer %opt{swap_buffer_target}
    "
    delete-buffer # delete the temporary scratch buffer
    buffer %opt{swap_buffer_target}
}

define-command -params .. fifo %{ evaluate-commands %sh{
     output=$(mktemp -d "${TMPDIR:-/tmp}"/kak-fifo.XXXXXXXX)/fifo
     mkfifo ${output}
     ( eval "$@" > ${output} 2>&1 & ) > /dev/null 2>&1 < /dev/null

     printf %s\\n "evaluate-commands -try-client '$kak_opt_toolsclient' %{
               edit! -fifo ${output} *fifo*
               hook -always -once buffer BufCloseFifo .* %{ nop %sh{ rm -r $(dirname ${output}) } }
           }"
}}

