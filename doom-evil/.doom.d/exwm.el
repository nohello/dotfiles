;;; ~/dotfiles/doom/.doom.d/exwm.el -*- lexical-binding: t; -*-

(use-package! exwm
  :config

  (defun exwm-config-personal ()
    "Default configuration of EXWM."
    ;; Set the initial workspace number.
    (unless (get 'exwm-workspace-number 'saved-value)
      (setq exwm-workspace-number 4))
    ;; Make class name the buffer name
    (add-hook 'exwm-update-class-hook
              (lambda ()
                (exwm-workspace-rename-buffer exwm-class-name)))

    ;; Global keybindings.
    (setq exwm-input-global-keys
          `(
            ;; 's-r': Reset (to line-mode).
            ([?\s-r] . exwm-reset)
            ;; 's-&': Launch application.
            ([?\s-z] . (lambda (command)
                        (interactive (list (read-shell-command "$ ")))
                        (start-process-shell-command command nil command)))
            ;; 's-N': Switch to certain workspace.
            ,@(mapcar (lambda (i)
                        `(,(kbd (format "s-%d" i)) .
                          (lambda ()
                            (interactive)
                            (exwm-workspace-switch-create ,i))))
                      (number-sequence 0 9)))))



  (setq display-time-default-load-average nil)
  (display-time-mode t)
  (require 'exwm-systemtray)
  (exwm-systemtray-enable)
  (require 'exwm)
  (require 'exwm-config)
  (exwm-config-personal)
  (exwm-enable)
  (setq exwm-workspace-number 4)
  (setq exwm-manage-configurations '((t char-mode t)))
  (add-hook 'exwm-update-class-hook
            (lambda ()
              (unless (or (string-prefix-p "sun-awt-X11-" exwm-instance-name)
                          (string= "gimp" exwm-instance-name)
                          (string= "intellij" exwm-instance-name))
                (exwm-workspace-rename-buffer exwm-class-name))))
  (add-hook 'exwm-update-title-hook
            (lambda ()
              (when (or (not exwm-instance-name)
                        (string-prefix-p "sun-awt-X11-" exwm-instance-name)
                        (string= "gimp" exwm-instance-name)
                        (string= "jetbrains-idea-ce" "idea"))
                (exwm-workspace-rename-buffer exwm-title))))
  ;; Unset global key: "s-t"
  (global-set-key (kbd "s-t") nil)
  ;; Use "s-t" as an exwm prefix key.
  (push ?\s-t exwm-input-prefix-keys)
  (exwm-input-set-key (kbd "<s-tab>") 'evil-switch-to-windows-last-buffer)
  (exwm-input-set-key (kbd "s-q") 'kill-buffer-and-window)
  (exwm-input-set-key (kbd "s-c") 'evil-window-delete)
  (exwm-input-set-key (kbd "s-r") 'evil-window-rotate-downwards)
  (exwm-input-set-key (kbd "s-R") 'evil-window-rotate-upwards)
  (exwm-input-set-key (kbd "s-u") 'winner-undo)
  (exwm-input-set-key (kbd "s-U") 'winner-redo)
  (exwm-input-set-key (kbd "s-d") 'dmenu)
  (exwm-input-set-key (kbd "s-v") 'split-window-right)
  (exwm-input-set-key (kbd "s-s") 'split-window-below)
  (exwm-input-set-key (kbd "s-p") 'helm-projectile-switch-project)
  (exwm-input-set-key (kbd "s-,") 'helm-mini)
  (exwm-input-set-key (kbd "s-.") 'helm-find-files)
  (exwm-input-set-key (kbd "s-x") 'helm-M-x)
  (exwm-input-set-key (kbd "s-e") 'doom/window-maximize-buffer)
  (exwm-input-set-key (kbd "s-W") 'evil-window-prev)
  (exwm-input-set-key (kbd "s-w") 'evil-window-next)
  (exwm-input-set-key (kbd "s-j") 'evil-window-next)
  (exwm-input-set-key (kbd "s-k") 'evil-window-prev)
  (exwm-input-set-key (kbd "s-f") 'next-buffer)
  (exwm-input-set-key (kbd "s-b") 'next-buffer)
  (exwm-input-set-key (kbd "<s-next>") 'next-buffer)
  (exwm-input-set-key (kbd "<s-prior>") 'previous-buffer)
  (exwm-input-set-key (kbd "<s-down>") 'evil-window-next)
  (exwm-input-set-key (kbd "<s-up>") 'evil-window-prev)
  (exwm-input-set-key (kbd "<s-right>") 'next-buffer)
  (exwm-input-set-key (kbd "<s-left>") 'previous-buffer)
  (exwm-input-set-key (kbd "s-m") 'magit-status)
  (exwm-input-set-key (kbd "<s-return>") '+eshell/toggle)
  (exwm-input-set-key (kbd "s-]") 'evil-window-increase-width)
  (exwm-input-set-key (kbd "s-[") 'evil-window-decrease-width)
  (exwm-input-set-key (kbd "s-}") 'evil-window-increase-height)
  (exwm-input-set-key (kbd "s-{") 'evil-window-decrease-height)
  (exwm-input-set-key (kbd "s-h") 'helm-exwm)
  (exwm-input-set-key (kbd "s-n") 'org-capture)
  (exwm-input-set-key (kbd "s--") 'dired)
  (exwm-input-set-key (kbd "s-i") 'doom/find-file-in-private-config)
  (exwm-input-set-key (kbd "s-y") 'helm-spotify-plus)
  (exwm-input-set-key (kbd "s-o") 'org-agenda)
  (exwm-input-set-key (kbd "s-t w") 'exwm-workspace-switch)
  (exwm-input-set-key (kbd "s-t m") 'exwm-workspace-move)
  (exwm-input-set-key (kbd "s-t s") 'ace-swap-window)
  (exwm-input-set-key (kbd "s-t d") 'ace-delete-window))
