;;; ~/dotfiles/doom/.doom.d/kak.el -*- lexical-binding: t; -*-

(use-package! kakoune
  ;; Having a non-chord way to escape is important, since key-chords don't work in macros
  :bind ("C-z" . ryo-modal-mode)
  :hook (after-init . my/kakoune-setup)
  :config
  (defun ryo-enter () "Enter normal mode" (interactive) (ryo-modal-mode 1))
  (defun my/kakoune-setup ()
      "Call kakoune-setup-keybinds and then add some personal config."
      (kakoune-setup-keybinds)
      (setq ryo-modal-cursor-type 'box)
      (add-hook 'prog-mode-hook #'ryo-enter)
      (add-hook 'text-mode-hook #'ryo-enter)
      (ryo-modal-keys
       ("SPC")
       (":" helm-M-x)
       ("/" evil-ex-search-forward)
       (";" kakoune-deactivate-mark)
       ("#" comment-line)
       ("P" kakoune-paste-above)
       ("m" mc/mark-next-like-this)
       ("M" mc/skip-to-next-like-this)
       ("n" evil-ex-search-next)
       ("N" mc/skip-to-previous-like-this)
       ("M-m" mc/edit-lines)
       ("*" mc/mark-all-like-this)
       ("M-s" mc/split-region)
       ("C-u" evil-scroll-up :first '(deactivate-mark))
       ("C-d" evil-scroll-down :first '(deactivate-mark)))))

;; This overrides the default mark-in-region with a prettier-looking one,
;; and provides a couple extra commands
(use-package visual-regexp
  :ryo
  ("s" vr/mc-mark)
  ("?" vr/replace)
  ("M-/" vr/query-replace))

;; Emacs incremental search doesn't work with multiple cursors, but this fixes that
(use-package phi-search
  :bind
  (("C-s" . phi-search)
   ("C-r" . phi-search-backward)))

;; Probably the first thing you'd miss is undo and redo, which requires an extra package
;; to work like it does in kakoune (and almost every other editor).
(use-package undo-tree
  :config
  (global-undo-tree-mode)
  :ryo
  ("u" undo-tree-undo)
  ("U" undo-tree-redo)
  ("SPC u" undo-tree-visualize)
  :bind (:map undo-tree-visualizer-mode-map
              ("h" . undo-tree-visualize-switch-branch-left)
              ("j" . undo-tree-visualize-redo)
              ("k" . undo-tree-visualize-undo)
              ("l" . undo-tree-visualize-switch-branch-right)))

(global-set-key (kbd "<escape>") 'ryo-modal-mode)
(define-key ryo-modal-mode-map (kbd "<escape>") 'keyboard-quit)
(evil-set-initial-state 'prog-mode 'emacs)
(evil-set-initial-state 'text-mode 'emacs)


(setq ryo-modal-cursor-type nil)
(setq ryo-modal-cursor-color nil)
(setq-default cursor-type nil)
