;;; ~/dotfiles/doom/.doom.d/mu4e.el -*- lexical-binding: t; -*-

(require 'mu4e)

mu4e-attachment-dir "~/Downloads"

(setq user-mail-address "spacecorgi@pm.me"
user-full-name  "corgi")

;; Get mail
(setq mu4e-get-mail-command "mbsync protonmail")
mu4e-change-filenames-when-moving t   ; needed for mbsync
mu4e-update-interval 120             ; update every 2 minutes

;; Send mail
(setq message-send-mail-function 'smtpmail-send-it)
smtpmail-smtp-server "127.0.0.1"
smtpmail-stream-type 'starttls
smtpmail-smtp-service 1025

(add-to-list 'gnutls-trustfiles (expand-file-name "~/.config/protonmail/bridge/cert.pem"))
