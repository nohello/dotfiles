;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!

;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "smoogi"
      user-mail-address "spacecorgi@pm.me")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
(setq doom-font (font-spec :family "Iosevka" :size 14))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
;; (setq doom-theme 'doom-solarized-dark)
;; (setq doom-theme 'doom-solarized-light)
(setq doom-theme 'doom-dracula)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type 'visual)

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package' for configuring packages
;; - `after!' for running code after a package has loaded
;; ;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c g k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c g d') to jump to their definition and see how
;; they are implemented.

;; ADD-HOOK
(add-hook 'javascript- (setq tab-width 2))

;; SETQ
(setq mouse-autoselect-window t
      focus-follows-mouse t)
(setq doom-leader-alt-key "s-t")
(setq evil-multiedit-store-in-search-history t)
(setq evil-multiedit-follow-matches t)

;; MAP!
(map! :leader :desc "Switch to last buffer" "DEL" 'evil-switch-to-windows-last-buffer)
(map! :leader :desc "Interactive window navigation" "w i" '+hydra/window-nav/body)
(map! :leader :desc "Interactive text zoom" "t i" '+hydra/text-zoom/body)

(map! :map evil-normal-state-map "^" 'mark-whole-buffer)
(map! :map evil-normal-state-map "#" 'comment-line)
(map! :map evil-normal-state-map "U" 'evil-multiedit-restore)
(map! :map evil-normal-state-map "g j" 'evil-goto-line)
(map! :map evil-normal-state-map "g k" 'evil-goto-first-line)
(map! :map evil-normal-state-map "g h" 'move-beginning-of-line)
(map! :map evil-normal-state-map "g l" 'move-end-of-line)
(map! :map evil-normal-state-map "<RET>" 'evil-avy-goto-char-timer)
(map! :map evil-visual-state-map "<RET>" 'evil-avy-goto-char-timer)
(map! :map ivy-minibuffer-map [escape] 'minibuffer-keyboard-quit)
(map! :map swiper-map [escape] 'minibuffer-keyboard-quit)

;; Multi-cursor stuffs
(map! "M-j" 'evil-mc-make-and-goto-next-match)
(map! "M-k" 'evil-mc-make-and-goto-prev-match)
(map! "M-m" 'evil-mc-make-all-cursors)
(map! :map evil-visual-state-map "I" 'evil-mc-make-cursor-in-visual-selection-beg)
(map! :map evil-visual-state-map "A" 'evil-mc-make-cursor-in-visual-selection-end)
(map! :map evil-normal-state-map "M-s" '+multiple-cursors/evil-mc-toggle-cursor-here)
(map! :map evil-visual-state-map "M-s" '+multiple-cursors/evil-mc-toggle-cursor-here)
(map! :map evil-normal-state-map "M-t" '+multiple-cursors/evil-mc-toggle-cursors)
(map! :map evil-visual-state-map "M-t" '+multiple-cursors/evil-mc-toggle-cursors)

(map! "<C-mouse-4>" 'text-scale-increase)
(map! "<C-mouse-5>" 'text-scale-decrease)
(map! "<mode-line> <mouse-4>" 'previous-buffer)
(map! "<mode-line> <mouse-5>" 'next-buffer)

;; LOAD
;; (load! "./exwm.el")
;; (load! "./mu4e.el")

(provide 'config)
