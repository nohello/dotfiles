# Some settings
# function fish_title
#     true
# end
set fish_greeting

# set environment variable
set -gx PATH /snap/bin $PATH
# ENVIRONMENT VARIABLES
# EDITOR
set -gx EDITOR kak
set -gx VISUAL kak
set -gx PAGER kak
# DOOM emacs
# set -gx PATH $HOME/.emacs.d/bin $PATH
# set -gx PATH /snap/bin $PATH
# POSTGRES
# set -gx DATABASE_URL postgres://(whoami)
# GOLANG
# set -gx GOPATH $HOME/go
# set -gx PATH $GOPATH $GOPATH/bin $PATH
# set -gx PATH /usr/local/go/bin $PATH
# set -gx PATH ~/.local/bin $PATH
# NPM
# set -gx PATH ~/npm/bin/ $PATH
    
# ALIASES
alias ntp_set_date='sudo ntpdate asia.pool.ntp.org'
alias f='$VISUAL (rg --files --hidden | fzf)'
alias r='ranger'
alias mg='emacsclient -nw --eval "(magit-status)"'
alias fi="pacman -Slq | fzf --multi --preview 'pacman -Si {1}' | xargs -ro sudo pacman -S"
# drive command line
alias dlist='drive list'
alias dpush='drive push -no-prompt'
alias dpull='drive pull -no-prompt'
