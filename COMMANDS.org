#+TITLE: Commands

The following are collections of useful commands that save my day all the time.

* generic

#+BEGIN_SRC
du -h
df -h
cal
cal 3
cal june 584
date
date -ls
date "%Y %m %d"
ps ax | grep emacs
ps axc
pidof
pidgrep
top
htop
gtop
ip addr
#+END_SRC

* nmap

Find info about your current ip

#+BEGIN_SRC sh :results replace drawer :hlines yes
  ifconfig
  # or
  ip addr show
#+END_SRC

Scan devices in the same network

#+BEGIN_SRC sh :results replace drawer :hlines yes
  # Use your subnet here
  nmap -sP 192.168.1.10/24
#+END_SRC

* emacs

Solve org problems in emacs This is done by getting rid of old =*.elc=
and recompile emacs

#+BEGIN_SRC sh :results replace drawer :hlines yes
cd ~/.emacs.d/elpa/26.3/develop
find org*/*.elc -print0 | xargs -0 rm
#+END_SRC

Emacs systemd daemon

#+BEGIN_QUOTE
Systemd is the supported method of running applications at startup on most Linux distributions.
The following configuration file emacs.service will be included in the standard Emacs installation as of 26.1.
All you need to do is copy this to ~/.config/systemd/user/emacs.service.
#+END_QUOTE

#+BEGIN_SRC sh :results replace drawer :hlines yes
[Unit]
Description=Emacs text editor
Documentation=info:emacs man:emacs(1) https://gnu.org/software/emacs/

[Service]
Type=simple
ExecStart=/usr/bin/emacs --fg-daemon
ExecStop=/usr/bin/emacsclient --eval "(kill-emacs)"
Environment=SSH_AUTH_SOCK=%t/keyring/ssh
Restart=on-failure

[Install]
WantedBy=default.target
#+END_SRC

Then you should start the service and set it to automatically start for
all future boots:

#+BEGIN_SRC sh :results replace drawer :hlines yes
systemctl enable --user emacs
systemctl start --user emacs
#+END_SRC

Add this to =.emacs.d/init.el= to:
1. Solve emacs resizing problem
2. Solve signature problem

#+BEGIN_SRC elisp
(setq frame-resize-pixelwise t)
(setq package-check-signature nil)
#+END_SRC

* apt

List recently installed packages

#+BEGIN_SRC sh :results replace drawer :hlines yes
  grep " install " /var/log/dpkg.log
#+END_SRC

You could run this command to list only the recently installed package names

#+BEGIN_SRC sh :results replace drawer :hlines yes
awk '$3~/^install$/ {print $4;}' /var/log/dpkg.log
#+END_SRC

Install kernel-headers

#+BEGIN_SRC sh :results replace drawer :hlines yes
  $ sudo apt-get install linux-headers-$(uname -r) build-essential
#+END_SRC

Insert VBoxGuestAdditions.iso and run the followings

#+BEGIN_SRC sh :results replace drawer :hlines yes
  $ cd /media/cdrom0/
  $ sudo sh VBoxLinuxAdditions.run
#+END_SRC

Allow me to use vbox shared folder

#+BEGIN_SRC sh :results replace drawer :hlines yes
  sudo adduser $USER vboxsf
#+END_SRC

* zypper

List manually installed packages

#+BEGIN_SRC sh :results replace drawer :hlines yes
sudo less /var/log/zypp/history | grep "|command|"
#+END_SRC

#+RESULTS:
* pip

Python build essentials and co

#+BEGIN_SRC sh :results replace drawer :hlines yes
sudo apt-get install build-essential python3 python-dev python3-dev
#+END_SRC

sudo chmod a+rw /dev/ttyUSB0
* pacman

Query package list

#+BEGIN_SRC sh :results replace drawer :hlines yes
pacman -Qe | awk '{print $1}' > package_list.txt
#+END_SRC

* clisp/stumpwm

[[http://www.kaashif.co.uk/2015/06/28/hacking-stumpwm-with-common-lisp/index.html][Hacking stumpwm]]

Install *sbcl* first

Clone the repostory, and install quicklisp

#+BEGIN_SRC sh :results replace drawer :hlines yes
  git clone https://github.com/stumpwm/stumpwm.git
  curl -O https://beta.quicklisp.org/quicklisp.lisp
  sbcl --load $HOME/dotfiles/quicklisp.lisp
#+END_SRC

Install dependencies

#+BEGIN_SRC lisp
  (quicklisp-quickstart:install)

  (ql:add-to-init-file)

  ;; stumpwm dependencies
  (ql:quickload "clx")
  (ql:quickload "cl-ppcre")
  (ql:quickload "alexandria")

  ;; install stumpwm
  (ql:quickload "stumpwm")

  ;; install slime helper
  (ql:quickload "quicklisp-slime-helper")
  (ql:quickload :swank)
#+END_SRC

#+BEGIN_SRC
cd $HOME/stumpwm
./autogen.sh
./configure
make
sudo make install
#+END_SRC

* ffmpeg

[[https://blog.addpipe.com/converting-webm-to-mp4-with-ffmpeg/][Converting webm to mp4 using ffmpeg]]

The simplest catch all command to convert WebM to MP4 using FFmpeg is:

#+BEGIN_SRC sh :results replace drawer :hlines yes
ffmpeg -i video.webm video.mp4
#+END_SRC

When encoding video with H.264, the video quality can be controlled using a quantizer scale

#+BEGIN_SRC sh :results replace drawer :hlines yes
ffmpeg -i video.webm -crf 26 video.mp4
#+END_SRC

The following presets are available in descending order: ultrafast, superfast, veryfast, faster, fast, medium, slow, slower and veryslow.

#+BEGIN_SRC sh
ffmpeg -i video.webm -preset veryfast video.mp4
#+END_SRC
/gnome/desktop/wm/preferences/resize-with-right-button true
#+END_SRC

#+RESULTS:

Focus will follow mouse / or not

#+BEGIN_SRC  sh
gsettings set org.gnome.desktop.wm.preferences focus-mode 'sloppy'
# or
gsettings set org.gnome.desktop.wm.preferences focus-mode 'mouse'
#+END_SRC

* wget

Sometimes you want to create an offline copy of a site that you can take and view even without internet access. Using wget you can make such copy easily:

#+BEGIN_SRC sh
wget --mirror --convert-links --adjust-extension --page-requisites --no-parent
#+END_SRC

Explanation of the various flags:

    --mirror – Makes (among other things) the download recursive.
    --convert-links – convert all the links (also to stuff like CSS stylesheets) to relative, so it will be suitable for offline viewing.
    --adjust-extension – Adds suitable extensions to filenames (html or css) depending on their content-type.
    --page-requisites – Download things like CSS style-sheets and images required to properly display the page offline.
    --no-parent – When recursing do not ascend to the parent directory. It useful for restricting the download to only a portion of the site.

Alternatively, the command above may be shortened:

#+BEGIN_SRC sh
wget -mkEpnp
#+END_SRC

Note: that the last p is part of np (--no-parent) and hence you see p twice in the flags.


To mirror the entire website

#+BEGIN_SRC sh
wget --mirror --convert-links --adjust-extension --page-requisites --no-parent --no-clobber --continue
#+END_SRC
